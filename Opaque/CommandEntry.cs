﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
public class CommandEntry : Attribute{
    public String HelpText { get; set; }
}
