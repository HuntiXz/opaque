﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
/// <summary>
/// Awesome XNA to ascii keymap! 
/// All credit goes to Koen Bollen, Konsoul and the Phantom XNA game engine
/// </summary>
internal class KeyMap {
    [Flags]
    public enum Modifiers {
        NONE = 0x0,
        SHIFT = 0x1,
        ALT = 0x2,
        CTRL = 0x4
    }
    private static Dictionary<Keys, Dictionary<Modifiers, char>> keyMap;
    private static KeyMap instance = new KeyMap();
    private KeyMap() {
        keyMap = new Dictionary<Keys, Dictionary<Modifiers, char>>();
        keyMap[Keys.Space] = new Dictionary<Modifiers, char>();
        keyMap[Keys.Space][Modifiers.NONE] = ' ';
        keyMap[Keys.Space][Modifiers.SHIFT] = ' ';

        char[] specials = { ')', '!', '@', '#', '$', '%', '^', '&', '*', '(' };

        for (int i = 0; i <= 9; i++) {
            char c = (char)(i + 48);
            keyMap[(Keys)c] = new Dictionary<Modifiers, char>();
            keyMap[(Keys)c][Modifiers.NONE] = c;
            keyMap[(Keys)c][Modifiers.SHIFT] = specials[i];
        }

        for (char c = 'A'; c <= 'Z'; c++) {
            keyMap[(Keys)c] = new Dictionary<Modifiers, char>();
            keyMap[(Keys)c][Modifiers.NONE] = (char)(c + 32);
            keyMap[(Keys)c][Modifiers.SHIFT] = c;
        }

        keyMap[Keys.OemPipe] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemPipe][Modifiers.NONE] = '\\';
        keyMap[Keys.OemPipe][Modifiers.SHIFT] = '|';

        keyMap[Keys.OemOpenBrackets] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemOpenBrackets][Modifiers.NONE] = '[';
        keyMap[Keys.OemOpenBrackets][Modifiers.SHIFT] = '{';

        keyMap[Keys.OemCloseBrackets] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemCloseBrackets][Modifiers.NONE] = ']';
        keyMap[Keys.OemCloseBrackets][Modifiers.SHIFT] = '}';

        keyMap[Keys.OemComma] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemComma][Modifiers.NONE] = ',';
        keyMap[Keys.OemComma][Modifiers.SHIFT] = '<';

        keyMap[Keys.OemPeriod] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemPeriod][Modifiers.NONE] = '.';
        keyMap[Keys.OemPeriod][Modifiers.SHIFT] = '>';

        keyMap[Keys.OemSemicolon] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemSemicolon][Modifiers.NONE] = ';';
        keyMap[Keys.OemSemicolon][Modifiers.SHIFT] = ':';

        keyMap[Keys.OemQuestion] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemQuestion][Modifiers.NONE] = '/';
        keyMap[Keys.OemQuestion][Modifiers.SHIFT] = '?';

        keyMap[Keys.OemQuotes] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemQuotes][Modifiers.NONE] = '\'';
        keyMap[Keys.OemQuotes][Modifiers.SHIFT] = '"';

        keyMap[Keys.OemMinus] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemMinus][Modifiers.NONE] = '-';
        keyMap[Keys.OemMinus][Modifiers.SHIFT] = '_';

        keyMap[Keys.OemPlus] = new Dictionary<Modifiers, char>();
        keyMap[Keys.OemPlus][Modifiers.NONE] = '=';
        keyMap[Keys.OemPlus][Modifiers.SHIFT] = '+';
    }

    public static char getChar(Keys key, Modifiers mod) {
        if (!keyMap.ContainsKey(key))
            return '\0';
        if (!keyMap[key].ContainsKey(mod))
            return '\0';
        return keyMap[key][mod];
    }


}

