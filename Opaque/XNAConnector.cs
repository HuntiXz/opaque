﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

public class XNAConnector : DrawableGameComponent, OpaqueConnector {
    public Opaque opaque {
        get { return Opaque.Instance; }
    }
    ContentManager content;
    SpriteBatch sb;
    float animationY, restY;
    Texture2D backdrop;
    Color backdropColor;
    SpriteFont font;
    List<string> bufferHistory;

    public XNAConnector(Game game)
        : base(game) {
        content = new ResourceContentManager(game.Services, ContentRes.ResourceManager);
        bufferHistory = new List<string>();
    }
    public override void Initialize() {
        sb = new SpriteBatch(Game.GraphicsDevice);
        backdropColor = new Color(0, 0, 0, 160);
        restY = ((float)Game.GraphicsDevice.Viewport.Height) / 2.5f;
        base.Initialize();
    }
    protected override void LoadContent() {
        font = Game.Content.Load<SpriteFont>("OpaqueFont");
        backdrop = Game.Content.Load<Texture2D>("backdrop");
        base.LoadContent();
    }
    bool isAnimating;
    KeyboardState state, previous;
    double time, delta;
    int historyIndex = 0;
    public override void Update(GameTime gameTime) {
        base.Update(gameTime);
        delta = gameTime.TotalGameTime.TotalMilliseconds - time;
        state = Keyboard.GetState();
        if (KeyJustPressed(Keys.OemTilde)) {
            isAnimating = true;
        }
        if (isAnimating) {
            isAnimating = !AnimateConsole(opaque.active);
        }
        if (opaque.active) {
            bool shift = (state.IsKeyDown(Keys.LeftShift) || state.IsKeyDown(Keys.RightShift));
            if (state.IsKeyDown(Keys.Back) && delta > 75) {
                if (opaque.buffer.ToString() != "") {
                    time = gameTime.TotalGameTime.TotalMilliseconds;
                    opaque.BackSpace();
                }
            }
            if (KeyJustPressed(Keys.Delete)) {
                opaque.buffer.Clear();
            }
            Keys[] pressed = state.GetPressedKeys();
            foreach (Keys key in pressed) {
                if (!previous.IsKeyDown(key)) {
                    char c = KeyMap.getChar(key, shift ? KeyMap.Modifiers.SHIFT : KeyMap.Modifiers.NONE);
                    if (c != '\0')
                        opaque.buffer.Append(c);
                }
            }
            if (KeyJustPressed(Keys.Up)) {
                opaque.SetBufferToIndex(ref historyIndex);
                historyIndex++;
            }
            if (KeyJustPressed(Keys.Down)) {
                opaque.SetBufferToIndex(ref historyIndex);
                historyIndex--;
            }
            if (KeyJustPressed(Keys.Enter)) {
                historyIndex = 0;
                opaque.ConfirmCommand();
            }
            if (KeyJustPressed(Keys.Tab)) {
                opaque.AutoComplete();
            }
        }
        previous = state;
    }
    Keys? justpressed;
    private bool KeyJustPressed(Keys key) {
        if (key != justpressed && Keyboard.GetState().IsKeyDown(key)) {
            justpressed = key;
            return true;
        } else if (key == justpressed && Keyboard.GetState().IsKeyUp(key)) {
            justpressed = null;
        }
        return false;
    }
    float lerpvar = 0;
    
    private bool AnimateConsole(bool reverse = false) {
        int neg = (reverse) ? -1 : 1;
        lerpvar += 0.1f;
        animationY = (reverse) ? MathHelper.Lerp(restY, 0, lerpvar) : MathHelper.Lerp(0, restY, lerpvar);
        if (lerpvar >= 1) {
            opaque.active = !opaque.active;
            lerpvar = 0;
            return true;
        }
        
        return false;
    }
    public override void Draw(GameTime gameTime) {
        RasterizerState r = new RasterizerState();
        r.ScissorTestEnable = true;
        base.Draw(gameTime);
        sb.GraphicsDevice.ScissorRectangle = new Rectangle(0, 0, Game.GraphicsDevice.Viewport.Width, (int)animationY);
        sb.Begin(SpriteSortMode.Deferred, null, null, null, r);
        sb.Draw(backdrop, Vector2.Zero, null, backdropColor, 0.0f, Vector2.Zero, new Vector2(Game.GraphicsDevice.Viewport.Width, animationY), SpriteEffects.None, 0);
        sb.DrawString(font, opaque.display, new Vector2(40 ,animationY - font.MeasureString(opaque.display).Y - 20), Color.White);
        sb.DrawString(font, "> " + opaque.buffer + "|", new Vector2(30, animationY - 20), Color.White);
        sb.End();
    }
}
