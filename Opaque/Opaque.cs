﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

public class Opaque {
    
    private static Opaque _instance;
    private List<string> bufferHistory;
    private CommandGenerator generator;
    private SortedDictionary<string, Command> commandList;
    private Dictionary<object, SortedDictionary<string, Command>> instanceCommands;
    private string partialAutoComplete = null;
    private List<string> autoCompleteList = null;
    private object[] connectedInstances {
        get {
            return instanceCommands.Keys.ToArray();
        }
        set {
            foreach (object o in value) {
                instanceCommands.Add(o, new SortedDictionary<string, Command>());
            }
        }
    }
    public bool active;

    internal StringBuilder display;
    internal StringBuilder buffer;

    public static Opaque Instance {
        get {
            return _instance ?? (_instance = new Opaque());
        }
    }
    private Opaque() {
        display = new StringBuilder();
        buffer = new StringBuilder();
        commandList = new SortedDictionary<string, Command>();
        instanceCommands = new Dictionary<object, SortedDictionary<string, Command>>();
        bufferHistory = new List<string>();
        Init();
    }
    private void Init() {
        Log(@"                       ____                               
                      / __ \                              
                     | |  | |_ __   __ _  __ _ _   _  ___ 
                     | |  | | '_ \ / _` |/ _` | | | |/ _ \
                     | |__| | |_) | (_| | (_| | |_| |  __/
                      \____/| .__/ \__,_|\__, |\__,_|\___|
                            | |             | |           
                            |_|             |_|            by Nico 'Descalon' Glas

                     Type 'help' for a list of commands.");
        AddCommand(Command.CreateCommand("echo", "Prints text on the screen", delegate(CommandArgs args) {
            foreach (string s in args.parameters) {
                display.Append(s + " ");
            }
            display.AppendLine();
        }));
        AddCommand(Command.CreateCommand("help", "Displays this help text.", delegate(CommandArgs args) {
            if (args.IsEmpty()) {
                ListCommands(commandList);
            } else {
                string commandName = args.parameters[0].ToLower();
                if (commandList.ContainsKey(commandName)) {
                    Log("{0}:            {1}", commandName, commandList[commandName].helpText);
                } else {
                    Log("'{0}' is not recognized as a command", args.parameters[0]);
                }
            }
        }));
        AddCommand(Command.CreateCommand("alias", "Create a alias for a command", delegate(CommandArgs args) {
            if (args.IsEmpty()) {
                Log("How am I going to alias something without a command, you moron?");
            } else if (args.parameters.Length < 2) {
                Log("What am I aliasing?");
            } else {
                string command = args.parameters[0].ToLower();
                string alias = args.parameters[1];
                if (commandList.ContainsKey(command)) {
                    AddAlias(alias, commandList[command]);
                    Log("Added '{0}' as an alias for '{1}'", alias,args.parameters[0]);
                } else {
                    Log("'{0}'is not recognized as a command.", args.parameters[0]);
                }
            }

        }));
        AddCommand(Command.CreateCommand("instances", "Lists all instances", delegate(CommandArgs args) {
            if (connectedInstances.Length == 0)
                Log("No instances attached");
            foreach (var instance in connectedInstances) {
                Log(instance.GetType().Name.ToLower());
            }
        }));
        AddAlias("!", commandList["echo"]);
    }
    public void AddCommand(Command c) {
        commandList.Add(c.name.ToLower(), c);
    }
    public void AddAlias(string alias, Command toClone) {
        Command clone = toClone.Clone();
        clone.name = alias;
        clone.helpText = string.Format("See {0}.", toClone.name);
        AddCommand(clone);
    }

    public void AutoComplete() {
        partialAutoComplete = partialAutoComplete ?? buffer.ToString();
        if (autoCompleteList == null) {
            autoCompleteList = (from c in commandList
                                where c.Key.Length >= partialAutoComplete.Length && c.Key.Substring(0,partialAutoComplete.Length) == partialAutoComplete
                                select c.Key).ToList();
        }
        if (autoCompleteList.Count() > 0) {
            buffer.Clear();
            buffer.Append(autoCompleteList[0]);
            autoCompleteList.Remove(buffer.ToString());
        }
        if (autoCompleteList.Count() <= 0) {
            autoCompleteList = null;
        }
    }
    public void ConnectInstance(object o) {
        var members = o.GetType().GetMembers().Where(m => m.GetCustomAttributes(typeof(CommandEntry),false).Length > 0);
        if (members.Count() < 1) return;
        connectedInstances = new object[] { o };
        generator = new InstanceCommandGenerator(o);
        var commands = generator.Generate(members.ToArray());
        var dic = instanceCommands[o];
        foreach (var c in commands) {
            dic.Add(c.name, c);
        }
    }
    public void SetAllCommands() {
        Assembly assembly = Assembly.GetCallingAssembly();  
        var members = assembly.GetTypes().SelectMany(t => t.GetMembers()).Where(m => m.GetCustomAttributes(typeof(CommandEntry), false).Length > 0);
        generator = new StaticCommandGenerator();
        var commands = generator.Generate(members.ToArray());
        foreach (Command c in commands) {
            AddCommand(c);
        }
    }
    internal void SetBufferToIndex(ref int index) {
        buffer.Clear();
        index = (index >= bufferHistory.Count()) ? 0 : (index < 0) ? bufferHistory.Count() - 1 : index;
        buffer.Append(bufferHistory[index]);
    }
    private void ListCommands(SortedDictionary<string, Command> list) {
        string tabs = "                ";
        foreach (Command c in list.Values) {
            string tempTabs = tabs.Remove(0, c.name.Length);
            Log("{0}:{1}{2}", c.name, tempTabs, c.helpText);
        }
    }
    internal void BackSpace() {
        buffer.Remove(buffer.Length - 1, 1);
        partialAutoComplete = null;
    }
    internal void ConfirmCommand() {
        if (string.IsNullOrWhiteSpace(buffer.ToString())) {
            buffer.Clear();
            return;
        }
        if (string.IsNullOrEmpty(buffer.ToString())) return;
        bufferHistory.Insert(0, buffer.ToString());
        Log("> " + buffer);
        //display.AppendLine(); 
        CommandArgs args = CommandArgs.CreateCommandArgs(buffer);
        if (args.isInstanceCommand) {
            foreach (var item in connectedInstances) {
                if (item.GetType().Name.ToLower() == args.instanceName) {
                    if (String.IsNullOrEmpty(args.command)) {
                        ListCommands(instanceCommands[item]);
                    } else {
                        if (instanceCommands[item].ContainsKey(args.command))
                            instanceCommands[item][args.command].Invoke(args);
                        else
                            Log("{0} is not recognized as a command for {1}", args.command, args.instanceName);
                    }
                    break;
                }
            }
        }else if (commandList.ContainsKey(args.command)) {
            commandList[args.command].Invoke(args);
        }else if(EasterEggs()){
       
        } else {
            Log("'{0}' is not recognized as a command.", args.command);
        }
        buffer.Clear();
        partialAutoComplete = null;
    }
    public void Log(string s, params object[] args) {
        string format = string.Format(s, args);
        display.AppendLine(format);
    }
    private Dictionary<string, string> easterEggs;
    private bool EasterEggs() {
        return false;
    }
    
}
