﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Command {
    public delegate void CommandMethod(CommandArgs e);
    //CommandArgs arguments;
    public event CommandMethod functions;
    public string name;
    public string helpText;
    public static Command CreateCommand(string name, string helptext, CommandMethod c) {
        Command retval = new Command();
        retval.name = name;
        retval.helpText = (helptext[helptext.Count() - 1] == '.') ? helptext : helptext + '.';

        retval.functions += c;

        return retval;
    }


    public void Invoke(CommandArgs c) {
        if (functions != null) {
            functions(c);
        }
    }
    public Command Clone() {
        Command retval = new Command();
        retval.name = name;
        retval.helpText = helpText;
        retval.functions = functions;
        return retval;
    }
}
public abstract class CommandGenerator {
    public abstract Command[] Generate(MemberInfo[] members);
    protected abstract Command GenerateMethodCommand(MethodInfo method);
    protected abstract Command GeneratePropertyCommand(PropertyInfo property);
}
public class StaticCommandGenerator : CommandGenerator {

    public override Command[] Generate(MemberInfo[] members) {
        List<Command> retval = new List<Command>();

        foreach (var member in members) {
            if (member.MemberType == MemberTypes.Method) {
                var method = (MethodInfo)member;
                if (!method.IsStatic)
                    continue;
                retval.Add(GenerateMethodCommand(method));
            } else if (member.MemberType == MemberTypes.Property) {
                var property = (PropertyInfo)member;
                if (!property.GetGetMethod().IsStatic && !property.GetSetMethod().IsStatic)
                    continue;
                retval.Add(GeneratePropertyCommand((PropertyInfo)member));
            }
        }
        return retval.ToArray();
    }

    protected override Command GenerateMethodCommand(MethodInfo method) {
        CommandEntry entry = null;
        Command retval = new Command();

        entry = (from e in method.GetCustomAttributes(false)
                 where e is CommandEntry
                 select (CommandEntry)e).FirstOrDefault();

        MethodInfo capture = method;
        CommandEntry captureEntry = entry;

        return (Command.CreateCommand(method.Name.ToLower(), (entry != null) ? entry.HelpText : "No information available", delegate(CommandArgs args) {
            var parameters = (capture.GetParameters().Length > 0) ? new[] { args } : null;
            capture.Invoke(null, parameters);
        }));
    }

    protected override Command GeneratePropertyCommand(PropertyInfo property) {
        CommandEntry entry = null;
        Command retval = new Command();

        entry = (from e in property.GetCustomAttributes(false)
                 where e is CommandEntry
                 select (CommandEntry)e).FirstOrDefault();

        PropertyInfo capture = property;
        CommandEntry captureEntry = entry;

        return (Command.CreateCommand(property.Name.ToLower(), (entry != null) ? entry.HelpText : "No information available", delegate(CommandArgs args) {
            if (args.parameters != null) {
                var setValue = TypeDescriptor.GetConverter(capture.PropertyType).ConvertFromInvariantString(args.parameters[0]);
                var parameters = (capture.GetSetMethod().GetParameters().Length > 0) ? new[] { setValue } : null;
                capture.GetSetMethod().Invoke(null, parameters);
            }
            Opaque.Instance.Log("{0} : {1}", capture.Name, capture.GetGetMethod().Invoke(null, null).ToString());

        }));
    }
}
public class InstanceCommandGenerator : CommandGenerator {
    private object instance;
    public InstanceCommandGenerator(object instance) {
        this.instance = instance;
    }
    public override Command[] Generate(MemberInfo[] members) {
        List<Command> retval = new List<Command>();

        foreach (var member in members) {
            if (member.MemberType == MemberTypes.Method) {
                var method = (MethodInfo)member;
                if (method.IsStatic)
                    continue;
                retval.Add(GenerateMethodCommand(method));
            } else if (member.MemberType == MemberTypes.Property) {
                var property = (PropertyInfo)member;
                if (property.GetGetMethod().IsStatic && property.GetSetMethod().IsStatic)
                    continue;
                retval.Add(GeneratePropertyCommand((PropertyInfo)member));
            }
        }
        return retval.ToArray();
    }

    protected override Command GenerateMethodCommand(MethodInfo method) {
        CommandEntry entry = null;
        Command retval = new Command();

        entry = (from e in method.GetCustomAttributes(false)
                 where e is CommandEntry
                 select (CommandEntry)e).FirstOrDefault();

        MethodInfo capture = method;
        CommandEntry captureEntry = entry;

        return (Command.CreateCommand(method.Name.ToLower(), (entry != null) ? entry.HelpText : "No information available", delegate(CommandArgs args) {
            var parameters = (capture.GetParameters().Length > 0) ? new[] { args } : null;
            capture.Invoke(instance, parameters);
        }));
    }

    protected override Command GeneratePropertyCommand(PropertyInfo property) {
        CommandEntry entry = null;
        Command retval = new Command();

        entry = (from e in property.GetCustomAttributes(false)
                 where e is CommandEntry
                 select (CommandEntry)e).FirstOrDefault();

        PropertyInfo capture = property;
        CommandEntry captureEntry = entry;

        return (Command.CreateCommand(property.Name.ToLower(), (entry != null) ? entry.HelpText : "No information available", delegate(CommandArgs args) {
            if (args.parameters != null) {
                try {
                    var setValue = TypeDescriptor.GetConverter(capture.PropertyType).ConvertFromInvariantString(args.parameters[0]);
                    var parameters = (capture.GetSetMethod().GetParameters().Length > 0) ? new[] { setValue } : null;
                    capture.GetSetMethod().Invoke(instance, parameters);
                } catch (Exception e) {
                    Opaque.Instance.Log(e.Message);
                }
            }
            Opaque.Instance.Log("{0} : {1}", capture.Name, capture.GetGetMethod().Invoke(instance, null).ToString());
        }));
    }
}
public class CommandArgs {
    private static char[] specialChars = new[] { '$' };

    public string command;
    public string[] parameters;
    public string instanceName;
    public bool isInstanceCommand { get { return !string.IsNullOrEmpty(instanceName); } }

    public static CommandArgs CreateCommandArgs(String input) {
        CommandArgs retval = new CommandArgs();

        string[] commandandargs = input.Split(new char[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
        if (commandandargs.Count() > 1) {
            retval.parameters = commandandargs[1].Split(new char[] { ' ' });
        }
        retval.command = commandandargs[0].ToLower();
        if (retval.command[0] == '$') {
            string[] instanceAndCommand = retval.command.Split(new char[] { '.' }, 2, StringSplitOptions.RemoveEmptyEntries);
            retval.command = string.Empty;
            if (instanceAndCommand.Length > 1) {
                retval.command = instanceAndCommand[1].ToLower();
            }
            retval.instanceName = instanceAndCommand[0].TrimStart('$').ToLower();
        }
        return retval;
    }
    public static CommandArgs CreateCommandArgs(StringBuilder input) {
        return CreateCommandArgs(input.ToString());
    }
    public bool IsEmpty() {
        return parameters == null;
    }
}
